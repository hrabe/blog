/// <reference types="@sveltejs/kit" />

export enum Theme {
  Light = 'light',
  Dark = 'dark'
}
