---
title: How Did I Change Network Card For Acer Nitro 5
abstract: It's about the network card replacement and the issues I faced on.
isPublished: true
publishedOn: 2021-06-17T10:00:00+0200
slug: network_card_replacement
layout: blog
author: Michael Hrabě
cardImage: wifi-640x480.jpg
---

Acer Nitro 5 laptop is shipped with [Qualcomm Atheros QCA6174 802.11ac Wireless Adapter](https://www.qualcomm.com/products/qca6174a). At the very first look I didn't notice any potential issue on the laptop with Windows 10 OS preinstalled. As my OS preference is Linux distributions I created dual boot with Arch Linux + Gnome along Windows 10. 


Everything was OK until I start downloading node package dependencies via npm package manager. I was very frustrated because I developed an frontend application which has dependencies on node packages and I was unable to even build IT! 

After a couple investigations - playing YT videos, playing Twitch live streams and other web services - I noticed the network card was stable. That brought me to the fact that by downloading a lot of small files could potentially hit the network card limit. I tried that on Windows 10 with the same result.

Luckily I had a very old network card from Thinkpad x240 - [Intel® Dual Band Wireless-AC 7260](https://ark.intel.com/content/www/us/en/ark/products/75439/intel-dual-band-wireless-ac-7260.html). The change of the network card itself was pretty easy even for noobies. However I was still sceptical about the new wifi card as there could be some wifi driver record in BIOS whitelist. Wow, it worked like a charm on Linux but not on the latest Windows 10 as intel dropped the support.

I was not satisfied with half working solution so I decided to buy [Intel® Wireless-AC 9260](https://ark.intel.com/content/www/us/en/ark/products/99445/intel-wireless-ac-9260.html) which worked both Linux and the most recent Windows 10.
