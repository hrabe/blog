import { Theme } from '../global.d';
import storage from './localStorageStore'
// import { window } from '$app/env';
// enum Theme{
//   Light = 'light',
//   Dark = 'dark'
// }


// `preferences` will be:
//  - Synced with localStorage
//  - Accessible via pub/sub
let prefersTheme: Theme = null
if(!(typeof window === 'undefined')){
  prefersTheme = (window.matchMedia && window.matchMedia('(prefers-color-scheme: light)').matches) ? Theme.Light : Theme.Dark;
}
export const page = storage<number>('page', 1)
export const theme = storage<string>('theme', prefersTheme)
