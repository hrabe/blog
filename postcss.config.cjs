const tailwindcss = require("tailwindcss");
const autoprefixer = require("autoprefixer");
const postcssforloop = require('postcss-for');
const postcssnested = require('postcss-nested');
const postcsssimplevars = require('postcss-simple-vars');
const postcssrandom = require('postcss-random');
const postcsspercentage = require('postcss-percentage')
const postcsscustomproperties = require('postcss-custom-properties');
const postcsscalc = require('postcss-calc');
const postcssfunctions = require('postcss-functions');
const cssnano = require("cssnano");

const mode = process.env.NODE_ENV;
const dev = mode === "development";

const config = {
	plugins: [
        postcssforloop(),
        //Some plugins, like postcss-nested, need to run before Tailwind,
        tailwindcss(),
        postcssfunctions(),
        postcsscalc(),
        postcsssimplevars(),
        postcssnested(),
        postcsscustomproperties(),
        postcssrandom({
          round: 			   false,	// default: false
          noSeed: 		   true,	// default: false
          floatingPoint: 10 		// default: 5
        }),
        postcsspercentage({
          precision: 9,
          trimTrailingZero: true,
          floor: true
        }),
        //But others, like autoprefixer, need to run after,
        autoprefixer(),
        !dev && cssnano({
			preset: "default",
		})
    ],
    purge: [
      './src/**/*.html',
      './src/**/*.js',
    ],
};

module.exports = config;