---
title: Let's Dive Into PyScript
abstract: Deploy simple static website using PyScript
isPublished: true
publishedOn: 2024-01-27T20:00:00+0200
slug: pyscript_intro
layout: blog
author: Michael Hrabě
cardImage: PyScript-640x458.png
---

PyScript is relatively new approach to allow python community write python code natively in the browser. Python is shipped as WASM (web assembly). Firstly, I would like to create simple project leveraging python and javascript tooling. It should be exposed as simple static website with minimum overhead. 


Using plain css+html+js+python was not satisfied all my requirements. Thefore I picked the svelte kit.

# Svelte Kit

To use static site generator in svelte: [follow this link](https://kit.svelte.dev/docs/adapter-static).
