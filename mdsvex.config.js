import { defineMDSveXConfig as defineConfig } from 'mdsvex';
import remarkMath from 'remark-math';
import katex from 'katex';
import { visit } from 'unist-util-visit';
import rehypeKatex from 'rehype-katex';

const correct_hast_tree = () => (tree) => {
	visit(tree, 'text', (node) => {
		if (node.value.trim().startsWith('<')) {
			node.type = 'raw';
		}
	});
};

const katex_blocks = () => (tree) => {
	visit(tree, 'code', (node) => {
		if (node.lang === 'math') {

			const str = katex.renderToString(node.value, {
				displayMode: node.meta?.includes('isBlock') == true ? true : false,
				leqno: false,
				fleqn: false,
				throwOnError: true,
				errorColor: '#cc0000',
				strict: 'warn',
				output: 'mathml',
				trust: false,
				macros: { '\\f': '#1f(#2)' }
			});

			node.type = 'raw';
			node.value = '{@html `' + str + '`}';
		}
	});
};


export default defineConfig({
  extensions: ['.svelte.md', '.md', '.svx'],
  layout: {
    blog: './src/layouts/BlogLayout.svelte'
  },
  smartypants: {
    dashes: 'oldschool'
  },
  remarkPlugins: [remarkMath, katex_blocks],
  rehypePlugins: [correct_hast_tree, rehypeKatex]
});
