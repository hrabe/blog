---
title: Writing My Blog Page
abstract: Writing My Blog Page.
isPublished: true
publishedOn: 2021-07-20T09:15:00+0200
slug: writing_my_blog_page
layout: blog
author: Michael Hrabě
cardImage: my-web-640x480.jpg
---

At the first glance Svelte + Python combo seems as a right choice for my blog page. However I was not so impressed by python markdown parser which was not so powerfull without adding another extensions. I decided not to continue with python markdown parser as I was [reviewing](https://css-tricks.com/choosing-right-markdown-parser/) markdown parsers.

At that time I heard about Svelte as a template web framework which is not so heavy as Angular or React frameworks. I watched few videos about Svelte and gave it a chance. After introduction to Svelte I went for [Svelte kit](https://kit.svelte.dev/docs) (Sapper's successor) framework + mdsvex (markdown parser) + tailwindcss (css preprocessor) + typescript.


Just started a new svelte kit project by typing
```bat
npm init svelte@next blog
``` 
with typescript support selected. Adding tailwindcss and mdsvex to project (inspired by [rewriting-my-website-in-sveltekit](https://mattjennings.io/blog/rewriting-my-website-in-sveltekit)) were as easy as running

```bat
npx svelte-add tailwindcss
npx svelte-add mdsvex
```

For static site deployment I picked static adapter

```bat
npm i -D @sveltejs/adapter-static@next
```

as described [here](https://github.com/sveltejs/kit/tree/master/packages/adapter-static) 

```javascript
// svelte.config.js
import adapter from '@sveltejs/adapter-static';
export default {
	kit: {
		adapter: adapter({
			// default options are shown
			pages: 'build',
			assets: 'build',
			fallback: null
		})
	}
};
```

and then build it with
```
npx svelte-kit build
```
