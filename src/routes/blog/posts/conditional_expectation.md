---
title: Conditional Expectation
abstract: Interesting example of conditional expectation.
isPublished: true
publishedOn: 2025-02-01T22:00:00+0200
slug: conditional_expectation
layout: blog
author: Michael Hrabě
cardImage: tossing_coin.jpg
---

<!-- based on https://github.com/pngwn/mdsvex-math/blob/main/src/routes/index.svx -->

<svelte:head>
<!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.16.21/dist/katex.min.css" integrity="sha384-zh0CIslj+VczCZtlzBcjt5ppRcsAmDnRem7ESsYwWwg3m/OaJ2l4x7YBZl9Kxxib" crossorigin="anonymous"> -->

<link rel="stylesheet" href="/katex/katex.min.css">
</svelte:head>

<script lang="ts">
import MathKatex from '$lib/components/MathKatex.svelte';
import katex from 'katex';
</script>

## Tossing a Coin

Here is the first shot. Imagine that we flip a fair coin. What is the expected value of getting the first head?

Let's breakdown the problem. There are two events; getting head ```A(up)``` or tail ```B(down)``` and we know that <MathKatex math_expr="p(B) + p(A)=1" />. Assume that process of flipping a fair coin can be moddeled as a random variable <MathKatex math_expr="X" />. The probability of getting head is <MathKatex math_expr="p(X=A) = 0.5" /> and the probability of getting tail is <MathKatex math_expr="p(X=B) = 0.5" /> at any time.

From the expectation definition we know that the expected value of the random variable <MathKatex math_expr="X" /> is:

<MathKatex math_expr="E(X) = \sum_{'{i=1}'}^{'{n}'} t_i \cdot p_i (t_i|A)" is_block={true} />


where <MathKatex math_expr="t_i" /> is the value of time sample <MathKatex math_expr="1,2,\cdots,n" /> and <MathKatex math_expr="p_i (t_i|A)" /> is the probability of getting head at given time <MathKatex math_expr="t_i" />. 



> <details>
> <summary> Hint (Click)</summary>
> <span class='katexx'><MathKatex math_expr="E(X) = \sum_{'{i=1}'}^{'{n}'} t_i \cdot p_i (t_i|A) = \sum_{'{i=1}'}^{'{n}'} i \cdot \frac{1}{'{2^i}'}" is_block={true} /></span>
> <span class='katexx'><MathKatex math_expr="k = \frac{1}{2} \implies E(X) = \sum_{'{i=1}'}^{'{n}'} i \cdot k^i" is_block={true} /></span>
> by multiplying both sides by <MathKatex math_expr="k" /> and subtracting them, we get: <br/>
> <span class='katexx'><MathKatex math_expr="E(X) - k \cdot E(X) = \sum_{'{i=1}'}^{'{n}'} i \cdot k^i - k \cdot \sum_{'{i=1}'}^{'{n}'} i \cdot k^i" is_block={true} /></span>
> <span class='katexx'><MathKatex math_expr="E(X) - k \cdot E(X) = (k + k^2 + \cdots + k^{'{n-1}'}) - n \cdot k^{'{n+1}'} = k \cdot \frac{'{1-k^n}'}{'{1-k}'} - n \cdot k^{'{n+1}'}" is_block={true} /></span>
> by applying the limit <MathKatex math_expr="n \to \infty" /> we get: <br/>
> <span class='katexx'><MathKatex math_expr="\frac{1}{2} \cdot E(X) = \frac{1}{2} \cdot \frac{1-0}{'{1-\\frac{1}{2}}'}- 0 \implies E(X) = 2" is_block={true} /></span>


Well, there is even a smarter way to calculate the expected value of the random variable <MathKatex math_expr="X" />. We can use `a trick` with the conditional expectation.

> <details>
> <summary> Hint (Click)</summary>
>
> <span class='katexx'><MathKatex math_expr="E(X) = E(X:A) + E(X:B) = p(A) \cdot E(X | A) + p(B) \cdot E(X | B) = \frac{1}{2} + \frac{1}{2} \cdot (E(X) + 1)" is_block={true} /></span>
> <br/> therefore: <br/>
> <span class='katexx'><MathKatex math_expr="E(X) = \frac{1}{2} + \frac{1}{2} \cdot (E(X) + 1) \implies E(X) = 2" is_block={true} /> </span>
> </details>

<style lang="postcss">
    .katexx {
        display: flex;
        width: 100%;
        overflow-x: scroll;
        scrollbar-width: none;
    }
</style>