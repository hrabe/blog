---
title: Conditional Expectation Part 2
abstract: Interesting example of conditional expectation.
isPublished: true
publishedOn: 2025-02-06T21:20:00+0200
slug: conditional_expectation_part2
layout: blog
author: Michael Hrabě
cardImage: tossing_coin.jpg
---

<!-- based on https://github.com/pngwn/mdsvex-math/blob/main/src/routes/index.svx -->

<svelte:head>
<!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.16.21/dist/katex.min.css" integrity="sha384-zh0CIslj+VczCZtlzBcjt5ppRcsAmDnRem7ESsYwWwg3m/OaJ2l4x7YBZl9Kxxib" crossorigin="anonymous"> -->

<link rel="stylesheet" href="/katex/katex.min.css">
</svelte:head>

<script lang="ts">
import MathKatex from '$lib/components/MathKatex.svelte';
import katex from 'katex';
</script>

## Tossing a Coin

Here is the story. Let's imagine that we flip a fair coin. What is the expected value of getting two consecutive heads?

Let's breakdown the problem. There are two events; getting head ```A(up)``` or tail ```B(down)``` and we know that <MathKatex math_expr="p(B) + p(A)=1" />. Assume that process of flipping a fair coin can be moddeled as a random variable <MathKatex math_expr="X" />. The probability of getting head is <MathKatex math_expr="p(X=A) = 0.5" /> and the probability of getting tail is <MathKatex math_expr="p(X=B) = 0.5" /> at any time.

From the expectation definition we know that the expected value of the random variable <MathKatex math_expr="X" /> is:

<MathKatex math_expr="E(X) = \cdots" is_block={true} />

Before diving into expected value formulae, there is even a smarter way to calculate the expected value of the random variable <MathKatex math_expr="X" />. We can use `a trick` with the conditional expectation.

> <details>
> <summary> Hint 1 (Click)</summary>
>
> <span class='katexx'><MathKatex math_expr="E(X) = E(X:1(A), 2(A)) + E(X:1(A), 2(B)) + E(X:1(B), 2(A)) + E(X:1(B), 2(B))  " is_block={true} /></span>
> </details>

> <details>
> <summary> Hint 2 (Click)</summary>
>
> <span class='katexx'><MathKatex math_expr="E(X:1(A), 2(A)) + E(X:1(A), 2(B)) + E(X:1(B), 2(B)) = \frac{1}{4} \cdot 2  + \frac{1}{4} \cdot (2 + E(X)) + \frac{1}{4} \cdot (2 + E(X))" is_block={true} /></span>
> <span class='katexx'><MathKatex math_expr="E(X:1(B), 2(A)) = 3 \cdot \frac{1}{8} + \frac{1}{8} \cdot (E(X) + 3)" is_block={true} /></span>
> <br/> therefore: <br/>
> <span class='katexx'><MathKatex math_expr="\implies E(X) = 6" is_block={true} /> </span>
> </details>

<style lang="postcss">
    .katexx {
        display: flex;
        width: 100%;
        overflow-x: scroll;
        scrollbar-width: none;
    }
</style>