---
title: How Did I Installed Garuda Linux
abstract: How Did I Installed Garuda Linux on Acer Nitro 5 Laptop.
isPublished: true
publishedOn: 2021-06-30T10:00:00+0200
slug: archlinux_installation
layout: blog
author: Michael Hrabě
cardImage: linux-639x590.jpg
---

Having a new laptop Acer Nitro 5 with preinstalled Windows 10 I decided to create dual boot Windows 10/ArchLinux on my laptop Acer Nitro 5. Firstly I start googling if anyone else already have succeded with dual boot.

![Acer Nitro 5](https://www.techadvisor.com/cmsdata/reviews/3797546/acer_nitro_5_a517_review_design_thumb.jpg)


The laptop have one SSD with 1TB so I chose 200GB space for Arch Linux installation. I went to Disk Management on Windows to shrink the current partion size to create a new 200GB partion for upcoming installation. Upsss, I can't shrink the Windows partion. Why? 

![Disk Manager](https://i.imgur.com/zP3CnEs.png)

There could be more factors:

1. Enabled pagefile on SSD. Just [turn it off](https://www.thomas-krenn.com/en/wiki/Optimize_Windows_for_SSDs).
2. Disable System Restore
3. Disable Hibernation mode in your power options
4. Run the Disk Cleanup Wizard, making sure to remove the hibernation file and all restore points.
5. Reboot
6. If nothing helps keep google!


I managed to create a new empty partion with 200GB on it. Next you need to tweek BIOS settings a little bit.

* Change SATA mode to AHCI from "RST premium" in BIOS
* Turn off the secure boot option in Bios settings.

Great! I continued with downloading [ArchLinux image](https://archlinux.org/download/) using [qBittorrent client](https://www.qbittorrent.org/) and applying ```dd``` command to create bootable USB flash disk from image. 

Let's boot!

List block devices 

```bash
$ lsblk
```

Find out USB flash drive and run ```dd``` command to create bootable USB. An example:

```bash
dd bs=4M if=/path/to/archlinux/image/{image name} of=/dev/{usb device name} conv=fsync oflag=direct status=progress
```

Be carefull, device name and partion name are not the same!

Don't forget to disable secure boot and you are ready to go! The installation is pretty straightforward with UI installer. I chose Sway as Environment Desktop and only issue I have was typing password in network manager to connect to Wifi. I overcame this with ```iwctl``` command line tool.
