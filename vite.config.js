import { defineConfig } from 'vite';
import { sveltekit } from '@sveltejs/kit/vite';

export default defineConfig({
  plugins: [sveltekit()],
  // Ensure the root is set correctly if needed
  // root: 'src' // Uncomment and adjust if your structure is different
});