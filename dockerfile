#
# Testing node image for gitlab pipeline
# Example:
# docker build --tag node-docker .
# ocker run -t -i --privileged node-docker bash
#

FROM node:latest

ENV NODE_ENV=production

WORKDIR /app
COPY ["package.json", "package-lock.json*", "./"]
RUN npm install --production
COPY . .
