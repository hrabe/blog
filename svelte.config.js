import { mdsvex } from "mdsvex";
import mdsvexConfig from "./mdsvex.config.js";
import { vitePreprocess } from '@sveltejs/vite-plugin-svelte';
// svelte.config.js
import adapter from '@sveltejs/adapter-static';

/** @type {import('@sveltejs/kit').Config} */
const config = {
  extensions: ['.svelte', ...mdsvexConfig.extensions],

  // Consult https://github.com/sveltejs/svelte-preprocess
  // for more information about preprocessors
  preprocess: [ vitePreprocess({script: true}), mdsvex(mdsvexConfig)],

  kit: {
    // By default, `npm run build` will create a standard Node app.
    // You can create optimized builds for different platforms by
    // specifying a different adapter
    adapter: adapter({
        // default options are shown
        pages: 'build',
        assets: 'build',
        fallback: '404.html'
    }),
    prerender: {
        entries: ['*'],
    }
  }
};

export default config;
