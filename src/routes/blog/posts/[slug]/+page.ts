// src/routes/blog/[slug]/+page.js
import type { EntryGenerator } from './$types';

export const entries: EntryGenerator = () => {
	return [
		{ slug: 'archlinux_installation' },
		{ slug: 'conditional_expectation' },
		{ slug: 'conditional_expectation_part2' },
		{ slug: 'network_card_replacement' },
		{ slug: 'petshop_cfg' },
		{ slug: 'pyscript_intro' },
		{ slug: 'writing_my_blog_page' }
	];
};

export const prerender = true;

export async function load({ params }) {
	const post = await import(`../${params.slug}.md`);
	const { title, date } = post.metadata;
	const content = post.default;

	return {
		content,
		title,
		date
	};
}